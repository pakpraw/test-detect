from flask import Flask
from flask_restful import Resource,Api,reqparse
from flask_cors import CORS
import werkzeug, os
import base64
import cv2
import numpy as np
import pytesseract
from PIL import Image

app = Flask(__name__)
api = Api(app)
CORS(app)

UPLOAD_FOLDER = 'static/img'
parser = reqparse.RequestParser()
#parser.add_argument('file',type=werkzeug.datastructures.FileStorage, location='files')
parser.add_argument('filename')
parser.add_argument('file')

class DetectLot2(Resource):
        decorators=[]

        def post(self):
            data = parser.parse_args()
	    #print(data)
            if data['file'] == None:
                return {'data':'','message':'No file found','status':'error'}
            photo = data['file']
            if photo:
		imgdata = base64.b64decode(photo)
                filename = data['filename']
		print(filename)
		try:
		    print("save image")
		    image = open('static/img/'+filename,"wb")
		    image.write(imgdata)
		    image.close()
		except:
		    print("error upload")
                image = cv2.imread('static/img/'+filename)
                #print(img)
                #height, width = img.shape[:2]
                #start_row, start_col = int(height*.38),int(width*.20)
                #end_row, end_col = int(height*.58),int(width*.80)
                #image = img[start_row:end_row, start_col:end_col]
                #scale_percent_h = 50
                #scale_percent_w = 50
                #width = int(cropped.shape[1] * scale_percent_w / 100)
                #height = int(cropped.shape[0] * scale_percent_h/ 100)
                #dim = (width, height)
                #image = cv2.resize(cropped, dim, interpolation = cv2.INTER_AREA)
                text_file = open('static/output.txt',"wb" )
                gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) 
                ret,thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV)
                kernel2 = np.ones((4,4),np.uint8)
                dilation = cv2.dilate(thresh,kernel2,iterations = 1)
                text=pytesseract.image_to_string(dilation)
                text_file.write(str(text)+"\n")
                kernel = np.ones((20,20), np.uint8) 
                kernel2 = np.ones((6,6),np.uint8)
                erosion = cv2.erode(thresh,kernel2,iterations = 1)
                img_dilation = cv2.dilate(erosion, kernel, iterations=1) 
                im2, ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)                
                sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
                for i, ctr in enumerate(sorted_ctrs):  
                    x, y, w, h = cv2.boundingRect(ctr) 
                    roi = image[y:y+h+10, x:x+w] 
                    #cv2.rectangle(image,(x,y),( x + w, y + h ),(0,255,0),2) 
                    grayImage = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
                    (thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 127, 255, cv2.THRESH_BINARY)
                    kernel2 = np.ones((4,4),np.uint8)
                    dilation = cv2.dilate(blackAndWhiteImage,kernel2,iterations = 1)
                    text=pytesseract.image_to_string(dilation)
                    text_file.write(str(text)+"\n")
                text_file.close()
                text_file_1 = open('static/output.txt','r')
                s = text_file_1.read()
                H = s.replace("$"," ")
                number = 0
                #print(H)
                for i in H.split():
                    try:
                        number = float(i)
                        break
                    except:
                        continue
                text_file_1.close()
                print(number) 
                if number:
                    return {'number':number,'message':'photo uploaded','status':'success'}
                return {'number':'','message':'Something when wrong','status':'error'}
class HelloWorld(Resource):
	def get(self):
		print("hello im detect lot.")
		return {'text':'Hello i am Detect lot!'}
api.add_resource(HelloWorld,'/')
api.add_resource(DetectLot2,'/detectlot2')
if __name__ == '__main__':
	app.run(host='0.0.0.0',port=5000,debug=True)

